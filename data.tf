data "aws_vpc" "default_central" {
  provider = aws.central
  default = true
}

data "aws_subnet_ids" "all_central" {
  provider = aws.central
  vpc_id = data.aws_vpc.default_central.id
}

data "aws_availability_zones" "available_central" {
  provider = aws.central
  state = "available"
}

data "aws_ami" "ubuntu_central" {
  provider = aws.central
  most_recent = true

  filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
        name   = "virtualization-type"
        values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_vpc" "default_west" {
  provider = aws.west
  default = true
}

data "aws_subnet_ids" "all_west" {
  provider = aws.west
  vpc_id = data.aws_vpc.default_west.id
}

data "aws_availability_zones" "available_west" {
  provider = aws.west
  state = "available"
}

data "aws_ami" "ubuntu_west" {
  provider = aws.west
  most_recent = true

  filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
        name   = "virtualization-type"
        values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
