#!/bin/bash
echo "Starting plan"
terraform init
terraform plan > /dev/null

status=$?
if [ $status -eq 0 ]; then
                      echo "Applying plan..."
                      terraform apply --auto-approve

         else 
                      echo "Plan has failed"
                      exit 1
fi

status=$?
if [ $status -eq 0 ]; then
                      echo "Solution deployed flawlessly!"
                      echo "Access it via http://ni.fixers.com.ar"

         else
                      echo "There has been an error, exciting, check output."
                      exit 1
fi

exit 0
