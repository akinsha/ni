resource "aws_lb" "alb_central" {
  provider = aws.central
  name               = "${var.project_name}-web-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg_central.id]
  subnets            = data.aws_subnet_ids.all_central.ids
  enable_deletion_protection = false
}

resource "aws_lb_target_group" "target_group_central" {
  provider = aws.central
  name         = "${var.project_name}-target-group-alb"
  protocol     = "HTTP"
  port         = 80
  vpc_id = data.aws_vpc.default_central.id
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    path                = "/index.html"
    port                = 80
  }
}

resource "aws_lb_listener" "alb_central" {
  provider = aws.central
  load_balancer_arn = aws_lb.alb_central.arn
  port = "80"
  default_action {
    target_group_arn = aws_lb_target_group.target_group_central.arn
    type = "forward"
  }
}

resource "aws_lb_target_group_attachment" "web_tg1_central" {
  provider = aws.central
  count = length(aws_instance.web_central)
  target_group_arn = aws_lb_target_group.target_group_central.arn
  target_id = aws_instance.web_central[count.index].id
  port             = 80
}

resource "aws_lb" "alb_west" {
  provider = aws.west
  name               = "${var.project_name}-web-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg_west.id]
  subnets            = data.aws_subnet_ids.all_west.ids
  enable_deletion_protection = false
}

resource "aws_lb_target_group" "target_group_west" {
  provider = aws.west
  name         = "${var.project_name}-target-group-alb"
  protocol     = "HTTP"
  port         = 80
  vpc_id = data.aws_vpc.default_west.id
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    path                = "/index.html"
    port                = 80
  }
}

resource "aws_lb_listener" "alb_west" {
  provider = aws.west
  load_balancer_arn = aws_lb.alb_west.arn
  port = "80"
  default_action {
    target_group_arn = aws_lb_target_group.target_group_west.arn
    type = "forward"
  }
}

resource "aws_lb_target_group_attachment" "web_tg1_west" {
  provider = aws.west
  count = length(aws_instance.web_west)
  target_group_arn = aws_lb_target_group.target_group_west.arn
  target_id = aws_instance.web_west[count.index].id
  port             = 80
}
