resource "aws_security_group" "lb_sg_central" {
  provider = aws.central
  name        = "${var.project_name}-lb_sg"
  description = "Allow 80 inbound traffic all traffic ALB"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow_ssh_anywhere_central" {
  provider = aws.central
  name        = "${var.project_name}-allow_ssh_anywhere"
  description = "Allow ssh inbound traffic all traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow_http_anywhere_central" {
  provider = aws.central
  name        = "${var.project_name}-allow_http_anywhere"
  description = "Allow 80 inbound traffic all traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.lb_sg_central.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "lb_sg_west" {
  provider = aws.west
  name        = "${var.project_name}-lb_sg"
  description = "Allow 80 inbound traffic all traffic ALB"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow_ssh_anywhere_west" {
  provider = aws.west
  name        = "${var.project_name}-allow_ssh_anywhere"
  description = "Allow ssh inbound traffic all traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow_http_anywhere_west" {
  provider = aws.west
  name        = "${var.project_name}-allow_http_anywhere"
  description = "Allow 80 inbound traffic all traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.lb_sg_west.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
