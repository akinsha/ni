resource "aws_key_pair" "keypair_central" {
  provider = aws.central
  key_name   = "NI"
  public_key = file("./files/NI.pub")
}

resource "aws_key_pair" "keypair_west" {
  provider = aws.west
  key_name   = "NI"
  public_key = file("./files/NI.pub")
}
