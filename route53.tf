resource "aws_route53_health_check" "health_check_central" {
  provider = aws.central
  fqdn              = aws_lb.alb_central.dns_name
  port              = 80
  type              = "HTTP"
  resource_path     = "/index.html"
  failure_threshold = "1"
  request_interval  = "10"

  tags = {
    Name = "health_check_central"
  }
}

resource "aws_route53_record" "ni_central" {
  provider = aws.central
  zone_id = "Z2BRH2UFKBERDR"
  name    = "ni"
  type    = "CNAME"
  ttl     = "60"
  
  failover_routing_policy {
    type = "PRIMARY"
  }

  set_identifier = "ni_primary" 
  records        = [aws_lb.alb_central.dns_name]
  health_check_id = aws_route53_health_check.health_check_central.id
 
}

resource "aws_route53_health_check" "health_check_west" {
  provider = aws.west
  fqdn              = aws_lb.alb_west.dns_name
  port              = 80
  type              = "HTTP"
  resource_path     = "/index.html"
  failure_threshold = "1"
  request_interval  = "10"

  tags = {
    Name = "health_check_west"
  }
}

resource "aws_route53_record" "ni_west" {
  provider = aws.west
  zone_id = "Z2BRH2UFKBERDR"
  name    = "ni"
  type    = "CNAME"
  ttl     = "60"

  failover_routing_policy {
    type = "SECONDARY"
  }

  set_identifier = "ni_secondary"
  records        = [aws_lb.alb_west.dns_name]
  health_check_id = aws_route53_health_check.health_check_west.id

}
