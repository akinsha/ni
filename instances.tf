resource "aws_instance" "web_central" {
  provider = aws.central
  ami = data.aws_ami.ubuntu_central.id
  availability_zone = data.aws_availability_zones.available_central.names[count.index]
  count = 2
  instance_type = "t2.micro"
  key_name = aws_key_pair.keypair_central.key_name

  vpc_security_group_ids = [
    aws_security_group.allow_ssh_anywhere_central.id ,
    aws_security_group.allow_http_anywhere_central.id
  ]

  tags = {
    Name = "${var.project_name}-WebServer-${count.index + 1}"
  }

  user_data = file("user-data-nginx.txt")
}

resource "aws_instance" "web_west" {
  provider = aws.west
  ami = data.aws_ami.ubuntu_west.id
  availability_zone = data.aws_availability_zones.available_west.names[count.index]
  count = 2
  instance_type = "t2.micro"
  key_name = aws_key_pair.keypair_west.key_name

  vpc_security_group_ids = [
    aws_security_group.allow_ssh_anywhere_west.id ,
    aws_security_group.allow_http_anywhere_west.id
  ]

  tags = {
    Name = "${var.project_name}-WebServer-${count.index + 1}"
  }

  user_data = file("user-data-nginx.txt")
}
